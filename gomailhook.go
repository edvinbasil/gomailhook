package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {
	cfg := parseConf("config.json")
	http.HandleFunc("/", createHandler(&cfg))

	fmt.Println("Server started at", cfg.ListenAddr)
	log.Fatal(http.ListenAndServe(cfg.ListenAddr, nil))
}

func createHandler(config *Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		var mail_incoming ForwardEmail
		err := decoder.Decode(&mail_incoming)

		if err != nil {
			log.Println(err)
		}

		t, err := time.Parse("2006-01-02T15:04:05-0700", mail_incoming.Date)
		discordContent := mail_incoming.Text
		if mail_incoming.IsHTML {
			discordContent = "--Redacted HTML Body--"

		}
		jsonData2 := DiscordHookBody{
			Username: config.Hook.DisplayName,
			Avatar:   config.Hook.AvatarUrl,
			Embeds: []DiscordEmbed{
				{
					Title: config.Hook.EmbedTitle,
					Color: config.Hook.EmbedColor,
					Footer: DiscordFooter{
						Text: "Recieved",
					},
					Timestamp: t.UTC().Format(time.RFC3339),
					Fields: []DiscordField{
						{
							Name:   "Message ID",
							Value:  mail_incoming.MessageID,
							Inline: false,
						},
						{
							Name:   "From",
							Value:  mail_incoming.From.Text,
							Inline: true,
						},
						{
							Name:   "To",
							Value:  mail_incoming.To.Text,
							Inline: true,
						},
						{
							Name:   "Subject",
							Value:  mail_incoming.Subject,
							Inline: false,
						},
						{
							Name:   "Content",
							Value:  discordContent,
							Inline: false,
						},
					},
				},
			},
		}

		json_data, _ := json.Marshal(jsonData2)
		resp, err := http.Post(config.Hook.Url,
			"application/json", bytes.NewBuffer(json_data))
		if err != nil {
			log.Println(err)
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			print(err)
		}
		fmt.Fprintf(w, string(body))
	}
}
