
# Gomailhook

Gomailhook sends your emails as discord alerts. 

Actualy no. Gomailhook parses webhooks from [forwardemail.net](http://forwardemail.net/) and sends it as discord message using discord webhooks.
forwardemail does the heavylifting of receiving emails and converting them into webhooks.


## Usage/Examples

Copy the given `config.json.samle` to `config.json` and adjust the variables as necessary.
The fields are:

| Field | Description |
|-------|--------------|
| `listen_addr` | The address that the server listens on. This is a string of the form "address:port". The port can be specified alone using ":port". example: "127.0.0.1:8080" |
| `hook.avatar_url` | The url to the avatar image for your webhook messages |
| `hook.display_name` | The name shown as the sender for webhook messages |
| `hook.embed_title` | The title of the embed containing the email message |
| `hook.embed_color` | Th accent color of the embed. Hex colors need to be converted to decimal. See [the discord documentation](https://discord.com/developers/docs/resources/channel#embed-object) for more info |


Build the binary using

```sh
go build
```
And run it

```sh
./gomailhook
```

Point your forwardemail webhook address to gomailhook and start receiving notifications!



## Acknowledgements

 - [forwardemail.net](https://forwardemail.net/) for providing their awesome service
