package main

type ForwardEmail struct {
	From      ForwardEmailAddress `json:"from"`
	To        ForwardEmailAddress `json:"to"`
	Subject   string              `json:"subject"`
	Date      string              `json:"date"`
	Text      string              `json:"text"`
	MessageID string              `json:"messageId"`
	IsHTML    bool                `json:"html"`
}
type ForwardEmailAddress struct {
	Text string `json:"text"`
}
type DiscordHookBody struct {
	Embeds   []DiscordEmbed `json:"embeds"`
	Username string         `json:"username"`
	Avatar   string         `json:"avatar_url"`
}
type DiscordEmbed struct {
	Title     string         `json:"title"`
	Color     int32          `json:"color"`
	Fields    []DiscordField `json:"fields"`
	Footer    DiscordFooter  `json:"footer"`
	Timestamp string         `json:"timestamp"`
}
type DiscordFooter struct {
	Text string `json:"text"`
}
type DiscordField struct {
	Name   string `json:"name"`
	Value  string `json:"value"`
	Inline bool   `json:"inline"`
}
