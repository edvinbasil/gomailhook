package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type Config struct {
	ListenAddr string `json:"listen_addr"`
	Hook       struct {
		Url         string `json:"url"`
		AvatarUrl   string `json:"avatar_url"`
		DisplayName string `json:"display_name"`
		EmbedTitle  string `json:"embed_title"`
		EmbedColor  int32  `json:"embed_color"`
	}
}

func parseConf(configfile string) Config {
	var cfg Config
	readFile(&cfg, configfile)
	readEnv(&cfg)
	if cfg.Hook.Url == "" {
		log.Fatalln("No webhook url givein in DISCORD_MAILHOOK_URL env")
	}
	return cfg
}

func processError(err error) {
	fmt.Println(err)
	os.Exit(2)
}

func readFile(cfg *Config, configpath string) {
	f, err := os.Open(configpath)
	if err != nil {
		processError(err)
	}
	defer f.Close()

	decoder := json.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		processError(err)
	}
}

func readEnv(cfg *Config) {
	var webhook_url = os.Getenv("DISCORD_MAILHOOK_URL")
	if webhook_url != "" {
		cfg.Hook.Url = webhook_url
	}
}
